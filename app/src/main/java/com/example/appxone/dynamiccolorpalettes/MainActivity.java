package com.example.appxone.dynamiccolorpalettes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // This is the quick and easy integration path.
        // May not be optimal (since you're dipping in and out of threads)
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_top_header_bg);
        Palette.from(bitmap).maximumColorCount(3).generate(new Palette.PaletteAsyncListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onGenerated(Palette palette) {
                // Get the "vibrant" color swatch based on the bitmap
                Palette.Swatch vibrant = palette.getVibrantSwatch();
                if (vibrant != null) {
                    // Set the background color of a layout based on the vibrant color
//                    containerView.setBackgroundColor(vibrant.getRgb());
//                    // Update the title TextView with the proper text color
//                    titleView.setTextColor(vibrant.getTitleTextColor());
                    Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
                    window.setStatusBarColor(vibrant.getRgb());
                }
            }
        });

    }
}
